# Flashlight

Simple app for switching screen color and turn on/off device's flash

### Functionality

- Change screen color on a display tap
- Turn on/off device's flash

*Both functions are independent of each other*

### Screenshots

|  Launch Screen  |  Red Screen | Yellow Screen | Green Screen | Turn Off |
|:--------:|:--------:|:--------:|:--------:|:--------:|
|<img src="./images/launch_screen.png" width="120">|<img src="./images/red_screen.png" width="120">|<img src="./images/yellow_screen.png" width="120">|<img src="./images/green_screen.png" width="120">|<img src="./images/off_screen.png" width="120">|
